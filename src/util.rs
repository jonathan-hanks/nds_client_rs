use crate::{Buffer, GPSTime, Data, DataType, Channel};

pub fn split_by_seconds(input: Buffer) -> Result<Vec<Buffer>, Buffer> {
    if input.time.nano != 0 || input.stop().nano != 0 || input.samples() == 0 || input.channel.sample_rate < 1.0 || input.channel.data_type == DataType::Unknown {
        return Err(input);
    }
    Ok(match input.data {
        Data::Int16(v) => split_by_seconds_impl(input.channel, input.time, v),
        Data::Int32(v) => split_by_seconds_impl(input.channel, input.time, v),
        Data::Int64(v) => split_by_seconds_impl(input.channel, input.time, v),
        Data::Float32(v) => split_by_seconds_impl(input.channel, input.time, v),
        Data::Float64(v) => split_by_seconds_impl(input.channel, input.time, v),
        Data::Complex32(v) => split_by_seconds_impl(input.channel, input.time, v),
        Data::UInt32(v) => split_by_seconds_impl(input.channel, input.time, v),
        Data::Unknown(_) => { return Err(input); }
    })
}

fn split_by_seconds_impl<T>(channel: Channel, start: GPSTime, data: Vec<T>) -> Vec<Buffer>
    where
        T: Clone + Copy, Data: From<Vec<T>>
{
    let mut results = Vec::new();
    let mut cur = start;
    let samples_per_sec = channel.sample_rate as usize;

    data.chunks_exact(samples_per_sec).for_each(|ch| {
        let new_data = Vec::from(ch);

        results.push(Buffer {
            channel: channel.clone(),
            time: cur.clone(),
            data: Data::from(new_data),
        });

        cur.seconds += 1;
    });

    results
}

#[cfg(test)]
mod test {
    use crate::ChannelType;
    use super::*;

    fn sample_buffer1_mtrend() -> Buffer {
        Buffer {
            channel: Channel {
                name: "X1:TEST_1".to_string(),
                channel_type: ChannelType::MTrend,
                data_type: DataType::Int32,
                sample_rate: 1.0 / 60.0,
                gain: 1.0,
                slope: 1.0,
                offset: 0.0,
                units: "counts".to_string(),
            },
            time: GPSTime { seconds: 1000000000, nano: 0 },
            data: Data::from(vec![1i32, 2i32, 3i32]),
        }
    }

    fn sample_buffer1_raw() -> Buffer {
        let mut data = Vec::new();
        for i in 0..32 {
            data.push(i as i32);
        }
        Buffer {
            channel: Channel {
                name: "X1:TEST_1".to_string(),
                channel_type: ChannelType::Raw,
                data_type: DataType::Int32,
                sample_rate: 16.0,
                gain: 1.0,
                slope: 1.0,
                offset: 0.0,
                units: "counts".to_string(),
            },
            time: GPSTime { seconds: 1000000000, nano: 0 },
            data: Data::from(data),
        }
    }

    fn sample_buffer1_strend() -> Buffer {
        let mut data = Vec::new();
        for i in 0..32 {
            data.push(i as i32);
        }
        Buffer {
            channel: Channel {
                name: "X1:TEST_1".to_string(),
                channel_type: ChannelType::STrend,
                data_type: DataType::Int32,
                sample_rate: 1.0,
                gain: 1.0,
                slope: 1.0,
                offset: 0.0,
                units: "counts".to_string(),
            },
            time: GPSTime { seconds: 1000000000, nano: 0 },
            data: Data::from(data),
        }
    }

    #[test]
    fn test_split_by_seconds_fail()
    {
        let b = sample_buffer1_mtrend();
        assert!(split_by_seconds(b).is_err());
        let mut b = sample_buffer1_raw();
        b.time.nano = 1000000000 / 16;
        assert!(split_by_seconds(b).is_err());
    }

    #[test]
    fn test_split_by_seconds_ok_1()
    {
        let b = sample_buffer1_raw();
        match split_by_seconds(b) {
            Ok(out) => {
                assert_eq!(out.len(), 2);
                for i in 0..2 {
                    assert_eq!(out[i as usize].time.seconds, 1000000000 + i);
                    assert_eq!(out[i as usize].samples(), 16);
                    match &out[i as usize].data {
                        Data::Int32(v) => {
                            assert_eq!(v[0], (i * 16) as i32);
                        }
                        _ => panic!("Invalid data type found"),
                    }
                }
            }
            Err(_) => panic!("split_by_seconds should have passed"),
        }
    }

    #[test]
    fn test_split_by_seconds_ok_2()
    {
        let b = sample_buffer1_strend();
        match split_by_seconds(b) {
            Ok(out) => {
                assert_eq!(out.len(), 32);
                for i in 0..32 {
                    assert_eq!(out[i as usize].time.seconds, 1000000000 + i);
                    assert_eq!(out[i as usize].samples(), 1);
                    match &out[i as usize].data {
                        Data::Int32(v) => {
                            assert_eq!(v[0], i as i32);
                        }
                        _ => panic!("Invalid data type found"),
                    }
                }
            }
            Err(_) => panic!("split_by_seconds should have passed"),
        }
    }
}